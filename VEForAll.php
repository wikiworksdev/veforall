<?php

if ( function_exists( 'wfLoadExtension' ) ) {
	wfLoadExtension( 'VEForAll' );
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['VEForAll'] = __DIR__ . '/i18n';
	
	$wgExtensionMessagesFiles['VEForAllMagic'] = __DIR__ . '/VEForAll.i18n.magic.php';
	wfWarn(
		'Deprecated PHP entry point used for VEForAll extension. Please use wfLoadExtension ' .
		'instead, see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return true;
} else {
	die( 'This version of the VEForAll extension requires MediaWiki 1.25+' );
}
